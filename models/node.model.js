const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let NodeSchema = new Schema({
  techName: String,
  commonName: String,
  comments: String
});

module.exports = NodeSchema
