const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let AlarmSchema = new Schema({
  type: String,
  node: Schema.Types.ObjectId,
  recordingPath: String
})

module.exports = AlarmSchema