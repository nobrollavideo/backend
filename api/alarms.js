var express = require("express");
var router = express.Router();
const mongoose = require("mongoose");

let schema = require("../models/alarm.model");
let model = mongoose.model("Alarm", schema);

//Create
router.route("/").post((req, res) => {
    let alarm = new model(req.body);
    alarm
      .save()
      .then(alarm => {
        res.status(201).json(alarm);
      })
      .catch(err => {
        res.status(500).send("Add failed");
      });
  });
  
  //Read (all)
  router.route("/").get((_, res) => {
    model.find((e, r) => {
      if (e) {
        console.log(e);
        res.status(500);
      } else {
        res.status(200).json(r);
      }
    });
  });
  
  //Read (one)
  router.route("/:id").get((req, res) => {
    model.findById(req.params.id, (e, r) => {
      if (e) {
        console.log(e);
        res.status(500);
      } else {
        res.status(200).json(r);
      }
    });
  });
  
  //Update
  router.route("/:id").put((req, res) => {
    model.findById(req.params.id, (e, alarm) => {
      if (e) {
        console.log(e);
        res.status(500);
      } else if (!alarm) {
        res.status(400).send("resource not found");
      } else {
        alarm.type = req.body.type;
        alarm.node = req.body.node;
        alarm.recordingPath = req.body.recordingPath;
      }
      alarm
        .save()
        .then(alarm => {
          res.status(201).json(alarm);
        })
        .catch(err => {
          res.status(500).send("update failed");
        });
    });
  });
  
  //Delete
  router.route("/:id").delete((req, res) => {
    model.findByIdAndDelete(req.params.id, (e, r) => {
      if (e) {
        console.log(e);
        res.status(500);
      } else {
        res.status(200).send("ok");
      }
    });
  });
  
  module.exports = router;