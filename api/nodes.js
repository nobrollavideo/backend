var express = require("express");
var router = express.Router();
const mongoose = require("mongoose");

let schema = require("../models/node.model");
let model = mongoose.model("Node", schema);

//Create
router.route("/").post((req, res) => {
  let node = new model(req.body);
  node
    .save()
    .then(node => {
      res.status(201).json(node);
    })
    .catch(err => {
      res.status(500).send("Add failed");
    });
});

//Read (all)
router.route("/").get((_, res) => {
  model.find((e, r) => {
    if (e) {
      console.log(e);
      res.status(500);
    } else {
      res.status(200).json(r);
    }
  });
});

//Read (one)
router.route("/:id").get((req, res) => {
  model.findById(req.params.id, (e, r) => {
    if (e) {
      console.log(e);
      res.status(500);
    } else {
      res.status(200).json(r);
    }
  });
});

//Update
router.route("/:id").put((req, res) => {
  model.findById(req.params.id, (e, node) => {
    if (e) {
      console.log(e);
      res.status(500);
    } else if (!node) {
      res.status(400).send("resource not found");
    } else {
      node.techName = req.body.techName;
      node.commonName = req.body.commonName;
      node.comments = req.body.comments;
    }
    node
      .save()
      .then(node => {
        res.status(201).json(node);
      })
      .catch(err => {
        res.status(500).send("update failed");
      });
  });
});

//Delete
router.route("/:id").delete((req, res) => {
  model.findByIdAndDelete(req.params.id, (e, r) => {
    if (e) {
      console.log(e);
      res.status(500);
    } else {
      res.status(200).send("ok");
    }
  });
});

module.exports = router;
