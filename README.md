# Backend setup & use

## required

* [mongodb](https://docs.mongodb.com/manual/administration/install-community/)
* npm
* nodemon:

```bash
npm install -g nodemon # install nodemon
```

## settup

```bash
# do once
mkdir -p /data/db # create data file location
mongod # start mongodb
mongo # connect to the database server
> use nss # create the database
> exit
```

## start the server

``` bash
mongod
```

``` bash
nodemon server
```

## using with Axios.js

*I use axios to make http/rest calls.*

install axios

```bash
npm install axios
```

import axios

```javascript
import axios from 'axios';
```

use axios to make a rest call

*here is a GET call and a POST call but you can use any call listed below*

```javascript
// get all nodes
axios.get('http://localhost:4000/nodes')
    .then(response => {
        console.log(response.data);
    })
    .catch(error => {
        console.log(error);
    })

// create a new node
var newNode = {
    "techName": "testing",
    "commonName": "test",
    "comments": "this is a test",
}

axios.post('http://localhost:4000/nodes', newNode)
    .then(response => {
        console.log(response.data);
    })
    .catch(error => {
        console.log(error);
    })
```

# Api

## Get all nodes

> GET http://localhost:4000/api/nodes

returns:

```json
[
    {
        "_id": "5dd454874c525c64bedc09e1",
        "techName": "testing",
        "commonName": "test",
        "comments": "this is a test",
        "__v": 0
    },
    {
        "_id": "5dd454964c525c64bedc09e2",
        "techName": "testing 2",
        "commonName": "second test",
        "comments": "this also is a test",
        "__v": 0
    }
]
```

## Get one node

> GET localhost:4000/api/nodes/5dd454874c525c64bedc09e1

returns:

```json
{
    "_id": "5dd454874c525c64bedc09e1",
    "techName": "testing",
    "commonName": "test",
    "comments": "this is a test",
    "__v": 0
}
```

## Create a node

> POST localhost:4000/api/nodes
>
> body:
>
> {
>
>"techName": "testing",
>
>"commonName": "test",
>
>"comments": "this is a test"
>
>}

returns:

```json
{
    "_id": "5dd454964c525c64bedc09e2",
    "techName": "testing",
    "commonName": "test",
    "comments": "this is a test",
    "__v": 0
}
```

## Update a node

>PUT localhost:4000/api/nodes/5dd454964c525c64bedc09e2
>
>body
>
> {
>
>"techName": "testing",
>
>"commonName": "updated string",
>
>"comments": "this is still a test"
>
>}

returns:

```json
{
    "_id": "5dd454964c525c64bedc09e2",
    "techName": "testing",
    "commonName": "updated string",
    "comments": "this is still a test",
    "__v": 0
}
```

## Delete a node

> DELETE localhost:4000/api/nodes/5dd454964c525c64bedc09e2

returns:

`ok`

## Get all alarms

> GET localhost:4000/api/alarms

returns:

```json
[
    {
        "_id": "5dd4582e4c525c64bedc09e3",
        "type": "nop",
        "node": "5dd44ad366bb5d5c7c39b13d",
        "recordingPath": "/dev/null",
        "__v": 0
    },
    {
        "_id": "5dd4583c4c525c64bedc09e4",
        "type": "sample",
        "node": "5dd44ad366bb5d5c7c39b13d",
        "recordingPath": "/dev/null",
        "__v": 0
    }
]
```

## Get one alarm

> GET localhost:4000/api/alarms/5dd4582e4c525c64bedc09e3

returns:

```json
{
    "_id": "5dd4582e4c525c64bedc09e3",
    "type": "nop",
    "node": "5dd44ad366bb5d5c7c39b13d",
    "recordingPath": "/dev/null",
    "__v": 0
}
```

## Create an alarm

> POST localhost:4000/api/alarms
>
> body:
>
>{
>
>"type": "sample",
>
>"node": "5dd44ad366bb5d5c7c39b13d",
>
>"recordingPath": "/dev/null"
>
>}

returns:

```json
{
    "_id": "5dd4583c4c525c64bedc09e4",
    "type": "sample",
    "node": "5dd44ad366bb5d5c7c39b13d",
    "recordingPath": "/dev/null",
    "__v": 0
}
```

## Update an alarm

> POST localhost:4000/api/alarms
>
> body:
>
>{
>
>"type": "sample",
>
>"node": "5dd44ad366bb5d5c7c39b13d",
>
>"recordingPath": "/home/testingmedia"
>
>}

returns:

```json
{
    "_id": "5dd4583c4c525c64bedc09e4",
    "type": "sample",
    "node": "5dd44ad366bb5d5c7c39b13d",
    "recordingPath": "/home/testingmedia",
    "__v": 0
}
```

## Delete an alarm

> DELETE localhost:4000/api/nodes/5dd4583c4c525c64bedc09e4

returns:

`ok`
