const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");
const mongoose = require("mongoose");
const PORT = 4000;

app.use(cors());
app.use(bodyParser.json());

// connect to db
mongoose.connect("mongodb://127.0.0.1:27017/nss", {
  useNewUrlParser: true
});
const connection = mongoose.connection;

connection.once("open", function() {
  console.log("MongoDB database connection established successfully");
});

// use apis
var nodeApi = require("./api/nodes");
app.use("/api/nodes", nodeApi);

var alarmApi = require("./api/alarms");
app.use("/api/alarms", alarmApi);

// start
app.listen(PORT, function() {
  console.log("Server is running on Port: " + PORT);
});
